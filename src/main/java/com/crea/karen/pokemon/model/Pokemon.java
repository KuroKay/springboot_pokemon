package com.crea.karen.pokemon.model;

import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "crea", type = "pokemon")
public class Pokemon {

  @Id
  @Field(type = FieldType.Long)
  private int id;

  @Field(type = FieldType.Text)
  private String name;

  @Field(type = FieldType.Text)
  private String element;

  @Field(type = FieldType.Text)
  private String image;

  public Pokemon() {
    super();
  }

  public Pokemon(int id, String name, String element) {
    this.id = id;
    this.name = name;
    this.element = element;
  }

  public String getName() {
    return name;
  }

  public String getElement() {
    return element;
  }

  public void setElement(String element) {
    this.element = element;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public void setName(String name) {
    this.name = name;
  }
}