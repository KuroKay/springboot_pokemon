package com.crea.karen.pokemon.controller;

import java.util.List;

import com.crea.karen.pokemon.dao.PokeballDao;
import com.crea.karen.pokemon.model.Pokeball;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(description = "API pour les opérations CRUD sur les produits.")
@RestController("/Pokeball")
public class PokeballController {

  @Autowired
  private PokeballDao pokeballDao;

  // Recherche Pokemon par le nom
  @GetMapping(value = "/Pokeball/{name}")
  public Pokeball showOnePokemonName(@PathVariable String name) {
    return pokeballDao.findByName(name);
  }

  // Afficher tous les produits
  @RequestMapping(value = "/AllPokeball", method = RequestMethod.GET)
  public List<Pokeball> showAllProduct() {
    return pokeballDao.findAll();
  }

}