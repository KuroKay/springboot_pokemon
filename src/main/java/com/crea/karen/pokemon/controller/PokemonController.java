package com.crea.karen.pokemon.controller;

import com.crea.karen.pokemon.dao.PokemonDao;
import com.crea.karen.pokemon.model.Pokemon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(description = "API pour les opérations CRUD sur les produits.")
@RestController("/")
public class PokemonController {

  @Autowired
  private PokemonDao pokemonDao;

  // Recherche Pokemon par le nom
  @GetMapping(value = "/Pokemon/{name}")
  public Pokemon showOnePokemonName(@PathVariable String name) {
    return pokemonDao.findByName(name);
  }

  // Recherche Pokemon par l' element
  @GetMapping(value = "/PokemonE/{element}")
  public Pokemon showOnePokemonElement(@PathVariable String element) {
    return pokemonDao.findByElement(element);
  }

  // Afficher tous les produits
  @RequestMapping(value = "/AllPokemon", method = RequestMethod.GET)
  public Page<Pokemon> showAllProduct() {
    return pokemonDao.findAll();
  }

}