package com.crea.karen.pokemon.dao;

import com.crea.karen.pokemon.model.Pokemon;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonDao extends ElasticsearchRepository<Pokemon, Integer> {
  public Page<Pokemon> findAll();

  public Pokemon findById(int id);

  public Pokemon findByName(String name);

  public Pokemon findByElement(String element);

}