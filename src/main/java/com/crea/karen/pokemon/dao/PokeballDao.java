package com.crea.karen.pokemon.dao;

import java.util.List;

import com.crea.karen.pokemon.model.Pokeball;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokeballDao extends JpaRepository<Pokeball, Integer> {
  public List<Pokeball> findAll();

  public Pokeball findById(int id);

  public Pokeball findByName(String name);

  public Pokeball findByElement(String decription);

}